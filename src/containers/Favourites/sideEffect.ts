import { getFavouriteGiphy } from "../../helpers/storage";
import { useState, useEffect } from 'react';
import { AxiosError, AxiosResponse, AxiosRequestConfig } from "axios";

import useDebounce from "../../components/debound";
import { GiphyApiRouteList } from "../../helpers/api/";
import { favouritesStoreType } from './type';
export const Favourite = (): favouritesStoreType => {
    const [loading, setloading] = useState<boolean | null>(null);
    const [giphy, setgiphy] = useState([]);
    const searchListIdsApi = (changePage = false) => {
        setloading(true);
        const listFavourite = getFavouriteGiphy();
        const config: AxiosRequestConfig = {
            params: { ids: listFavourite.toString() },
            data: {
            }
        };
        GiphyApiRouteList.searchListIds(config).then((res: AxiosResponse) => {
            const { data } = res.data;
            // console.log(data, "===data==");
            setgiphy(data);
        })
            .catch((err: AxiosError) => {
                // if (cb) cb(err);
            })
            .finally(() => {
                setloading(false)
            })
    }
    useEffect(() => {
        searchListIdsApi()
    },[])

    return {
        loading,
        giphy
    };
}