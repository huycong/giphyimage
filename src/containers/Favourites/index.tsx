import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { Grid } from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '@material-ui/core/Button';
import FavoriteIcon from '@material-ui/icons/Favorite';
import IconButton from '@material-ui/core/IconButton';

import { Favourite } from "./sideEffect";

const useStyles = makeStyles((theme: any) => ({
  root: {
    padding: theme.spacing(4)
  },
  imgGif: {
    width: '200px',
    height: '200px'
  },
  iconFa: {
    color: '#ea2c5a'
  }
}));

const Favourites = () => {
  const classes = useStyles();
  const searchEffect = Favourite();
  return (
    // <Suspense fallback={<div>Loading...</div>}>
    <div className={classes.root}>
      <Grid
        container
        spacing={4}
      >
        <Grid
          item
          lg={3}
          sm={6}
          xl={3}
          xs={12}
        >
        </Grid>

      </Grid>
      <div className={classes.root}>
        <Grid container spacing={3}>
          {searchEffect.giphy[0] && searchEffect.giphy.map((it: any, index: number) => {
            // const isLike = searchEffect.listFavourites.includes(it.id);
            return it && (
              <Grid item xs={3}>
                <img className={classes.imgGif} src={it.images.original.url} alt={it.title} />
                <IconButton  aria-label="add to favorites">
                  <FavoriteIcon
                    className={classes.iconFa}
                  />
                </IconButton>
                {/* <embed src={it.embed_url} type="" /> */}
              </Grid>
            )
          }

          )}
          {searchEffect.loading == true &&
            <>
              <Grid item xs={12}>
              </Grid>
              <Grid item xs={12}>
                <CircularProgress />
              </Grid>
            </>
          }
        </Grid>
      </div>

    </div>
    // </Suspense>
  );
};

export default Favourites;
