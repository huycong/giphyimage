export type dashboardStoreType = {
    loading: boolean | null,
    setq: any,
    giphy: any,
    setoffset: any,
    Loadmore: any,
    Like: any,
    listFavourites: string[],
    showAlert: any,
    messAlert: any,
    severity: any,
    listFavouritesIframe: any,
    isHover: any,
    setisHover: any

}