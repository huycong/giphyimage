import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { Grid } from '@material-ui/core';
import Input from '@material-ui/core/Input';
import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Skeleton from '@material-ui/lab/Skeleton';
import Paper from '@material-ui/core/Paper';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '@material-ui/core/Button';
import FavoriteIcon from '@material-ui/icons/Favorite';
import IconButton from '@material-ui/core/IconButton';
import Alert from '@material-ui/lab/Alert';
import Collapse from '@material-ui/core/Collapse';

import { Search } from './sideEffect';
import { dashboardStoreType } from './type';


const useStyles = makeStyles((theme: any) => ({
  root: {
    // padding: theme.spacing(4),
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
  imgGif: {
    width: '200px',
    height: '200px'
  },
  iconFa: {
    color: '#ea2c5a'
  }
}));

const Dashboard = (props: dashboardStoreType) => {
  const classes = useStyles();
  const searchEffect = Search();
  const {
    listFavourites,
    listFavouritesIframe,
    loading,
    giphy,
    showAlert,
    messAlert,
    severity,
    setq,
    setoffset,
    Loadmore,
    Like,
    isHover,
    setisHover
  } = searchEffect
  return (
    <div className={classes.root}>
      <Grid container>
        <Grid
          item
          xs={12}
        >
          <TextField
            disabled={loading == true}
            // {...params}
            label="Search input"
            margin="normal"
            variant="outlined"
            onChange={(e: any) => setq(e.target.value)}
          // InputProps={{ ...params.InputProps, type: 'search' }}
          />
        </Grid>


        <div className={classes.root}>
          <Grid container spacing={3}>
            {giphy[0] && giphy.map((it: any, index: number) => {
              const isLike = listFavourites.includes(it.id);
              let url = it.images["480w_still"].url
              if (!isLike && isHover === it.id && listFavouritesIframe[index] && listFavouritesIframe[index][1]) {
                url = listFavouritesIframe[index][1]
              }
              else if (isLike && listFavouritesIframe[index] && listFavouritesIframe[index][2]) {
                url = listFavouritesIframe[index][2]
              }
              return it && (
                <Grid item xs={3}>
                  <img
                    onMouseOver={e => {
                      setisHover(it.id)
                      // console.log(it.id);

                    }}
                    onMouseLeave={() => setisHover(null)}
                    className={classes.imgGif}
                    src={url}
                    // src={it.images["480w_still"].url}

                    alt={it.title} />
                  <IconButton onClick={e => Like(it.id, isLike)} aria-label="add to favorites">
                    <FavoriteIcon
                      className={isLike ? classes.iconFa : ''}
                    />
                  </IconButton>
                  {/* <embed src={it.embed_url} type="" /> */}
                </Grid>
              )
            })}
            {loading == true &&
              <>
                <Grid item xs={12}>
                </Grid>
                <Grid item xs={12}>
                  <CircularProgress />
                </Grid>
              </>
            }
          </Grid>
        </div>
      </Grid>
      <Grid
        item
        xs={12}
      >
        <Collapse in={showAlert}>
          {showAlert && <Alert severity={severity}>{messAlert}!</Alert>}
        </Collapse>
        <Button onClick={searchEffect.Loadmore} variant="contained" disabled={!!searchEffect.loading} color="secondary">
          Loadmore
        </Button>
      </Grid>
    </div>
  );
};

export default Dashboard;
