import { getFavouriteGiphy, setFavouriteGiphy, unSetFavouriteGiphy } from "../../helpers/storage";
import { useState, useEffect } from 'react';
import { AxiosError, AxiosResponse, AxiosRequestConfig } from "axios";
import gifFrames, { convertJpgToBlob } from "../../helpers/gitIFrame";

import lang from "../../helpers/lang";
import { alertType } from "../../helpers/config";
import useDebounce from "../../components/debound";
import { GiphyApiRouteList } from "../../helpers/api/";
import { dashboardStoreType } from './type';

export const Search = (): dashboardStoreType => {
    const [loading, setloading] = useState<boolean | null>(null);
    const [q, setq] = useState<string | null>(null);
    const [giphy, setgiphy] = useState<any>([]);
    const [limit, setlimit] = useState<number>(8);
    const [offset, setoffset] = useState<number>(0);
    const [listFavourites, setlistFavourites] = useState<any>([]);
    const [listFavouritesIframe, setlistFavouritesIframe] = useState<any>([]);
    const [isHover, setisHover] = useState<any>([]);


    const [showAlert, setshowAlert] = useState<any>(null);
    const [messAlert, setmessAlert] = useState<any>("");
    const [severity, setseverity] = useState<any>(null);

    const debouncedSearchTerm: string = useDebounce(q, 1000);

    const config: AxiosRequestConfig = {
        params: { q: debouncedSearchTerm, limit, offset: (offset) * limit },
        data: {
        }
    };

    const showAlertBytime = (ser: string, mess: string, time: number) => {
        setmessAlert(mess)
        setseverity(ser);
        setshowAlert(true);
        setTimeout(() => { setshowAlert(false) }, time)
    }


    const searchApi = (changePage = false) => {
        setloading(true);
        //set ListFavourite before
        const listFa: any = getFavouriteGiphy();
        setlistFavourites(listFa);
        GiphyApiRouteList.search(config).then((res: AxiosResponse) => {
            const { data } = res.data;
            // 
            gifFrames(data.map((ite) => ite.images.downsized_medium.url))
                .then(function (frameData: any) {
                    const listNewImg = frameData.map((ite: any) => {
                        return ite.map((item: any) => convertJpgToBlob(item.getImage()._obj))
                    })
                    if (!changePage) {
                        setlistFavouritesIframe(listNewImg);
                    }
                    else {
                        setlistFavouritesIframe([...listFavouritesIframe, ...listNewImg]);
                    }

                }).catch(() => {
                    showAlertBytime(alertType.ERR, lang.errorGetData, 3000)
                })
                .finally(() => setloading(false));

            if (changePage) {
                setgiphy([...giphy, ...data]);
            }
            else { setgiphy(data); }
            showAlertBytime(alertType.SUCC, lang.successGetData, 3000)
        })
            .catch((err: AxiosError) => {
                // if (cb) cb(err);
                setloading(false)
                showAlertBytime(alertType.ERR, lang.errorGetData, 3000)
            })
            .finally(() => {
                // setloading(false);
            })
    }

    const Loadmore = () => {
        setoffset(offset + 1)
    }

    const Like = (id: string, isLike: boolean) => {
        if (!isLike) {
            setFavouriteGiphy(id);
            const newList = ([...new Set([...listFavourites, id])]);
            setlistFavourites(newList)
            showAlertBytime(alertType.SUCC, lang.likeSuccess, 3000)
        } else {
            unSetFavouriteGiphy(id);
            const newList = listFavourites.filter((i: string) => i !== id);
            setlistFavourites(newList)
            showAlertBytime(alertType.WARN, lang.unLikeSuccess, 3000)
        }
    }

    useEffect(() => {
        if (q && limit) {
            searchApi()
        }
    }, [debouncedSearchTerm, limit]);

    useEffect(() => {
        if (q && limit) {
            const changePage = true;
            searchApi(changePage)
        }
    }, [offset]);

    return {
        listFavourites,
        listFavouritesIframe,
        loading,
        giphy,
        showAlert,
        messAlert,
        severity,
        setq,
        setoffset,
        Loadmore,
        Like,
        isHover,
        setisHover
    };
}