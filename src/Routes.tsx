import React, { Suspense, lazy } from 'react';
import { Switch, Redirect, withRouter } from 'react-router-dom';

import { RouteWithLayout } from './components';
import MainLayout from './layouts/Main';
// const RouteWithLayout = lazy(() => import('./components/RouteWithLayout'));
// const MainLayout = lazy(() => import('./layouts/Main'));

const Dashboard = lazy(() => import('./containers/Dashboard'));
const Favourites = lazy(() => import('./containers/Favourites'));


const Routes = (props: { location: any }) => {
  const path = props?.location?.pathname
  return (
    <MainLayout>
      <Suspense fallback={<div>Loading...</div>}>
        <Switch>
          <RouteWithLayout
            component={Dashboard}
            exact
            // layout={MainLayout}
            path="/"
          />
          <RouteWithLayout
            component={Dashboard}
            exact
            // layout={MainLayout}
            path="/dashboard"
          />
          <RouteWithLayout
            component={Favourites}
            exact
            // layout={MainLayout}
            path="/favourites"
          />
        </Switch>
      </Suspense>
    </MainLayout>
  );
};

export default withRouter(Routes);
