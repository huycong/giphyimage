import gifFrames from 'gif-frames';

export const convertJpgToBlob = (data: any) => {
    if(!data) return null;
    var bytes = new Uint8Array(data);
    const blob = new Blob([bytes.buffer], { type: "image/jpg" });
    return URL.createObjectURL(blob);
}

export default (url: string[]) => {
    return Promise.all(url.map((i: string) => {
        return gifFrames({ url: i, frames: '0-2', outputType: 'jpg', cumulative: true })
        // .then(function (frameData) {
        //     console.log(frameData, "===frameData==");

        // })
    }));
}