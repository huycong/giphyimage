import { language } from "../config"
const lang = {
    successGetData: {
        en: "success to get data!"
    },
    errorGetData: {
        en: "error to get data!"
    },
    likeSuccess: {
        en: "favourite done!"
    },
    unLikeSuccess: {
        en: "unlike done!"
    },
}
export default {
    successGetData: lang.successGetData[language],
    errorGetData: lang.errorGetData[language],
    likeSuccess: lang.likeSuccess[language],
    unLikeSuccess: lang.unLikeSuccess[language],
}