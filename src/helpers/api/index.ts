import { giphyV1 as giphyV1Url, giphyV1Api_key } from "../config";
import axios, { AxiosRequestConfig, AxiosResponse, AxiosError } from "axios";
const instance = axios.create({
    //   baseURL: giphyV1Url,
});

export default instance;
export type GiphyApiRouteListType = {
    search(payload: AxiosRequestConfig ): any,
    searchListIds(payload: AxiosRequestConfig ): any,
};

export const GiphyApiRouteList: GiphyApiRouteListType = {
    search: (payload: AxiosRequestConfig) => {
        payload.params = { ...payload.params, api_key: giphyV1Api_key }
        return instance.get(`${giphyV1Url}/gifs/search`, payload)
    },
    searchListIds: (payload: AxiosRequestConfig) => {
        payload.params = { ...payload.params, api_key: giphyV1Api_key }
        return instance.get(`${giphyV1Url}/gifs`, payload)
    },

}
