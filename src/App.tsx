import React, { Component } from 'react';
import { Router, BrowserRouter } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import { ThemeProvider } from '@material-ui/styles';
// import 'react-perfect-scrollbar/dist/css/styles.css';
// import './assets/scss/index.scss';
import Routes from './Routes';
import theme from './helpers/theme';

const browserHistory = createBrowserHistory();

export default class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <ThemeProvider theme={theme}>
          <Router history={browserHistory}>
            <Routes />
          </Router>
        </ThemeProvider>
      </BrowserRouter>
    );
  }
}

